# Библиотека локализаций #

1. [Открываем гугл док](https://docs.google.com/spreadsheets/d/14inhPXIGcEfE2mMIJT80JKmYVstMNAvs6Hk3PzmSY_A/edit#gid=0) и экспортируем себе на компьютер как эксельку

Разделы, которые необходимо локализовать, выражены вкладками в эксельке

Локализация приложений:

	- iOS app : приложение для iOS
	- OS X App : приложение для OS X

Метаданные приложений:

	- iTunesConnect Metadata : текстовое описание того, как приложение выглядит в iOS App Store
	- iOS Screenshot texts
	- OS X Metadata
	- Keywords
	- Android metadata

# Как работать # 

Скачиваем эксельку и обращаем внимание на вышеуказанные вкладки



### Зачем этот репозиторий? ###

* Чтобы сразу вставлять локализацию в приложение
* При этом разработчик не будет тратить лишнего времени на вбивание нужных локализованных строк

### Какой результат должен быть от вас ###

Три вещи
	- Экселька с метаданными

### Who do I talk to? ###

Almas